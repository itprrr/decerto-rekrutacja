import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;

@ExtendWith(MockitoExtension.class)
class IntegerDataJoinerImplTest {

    private final DataJoiner<Integer> dataJoiner = new DataJoinerImpl<>();

    @Test
    void testJoinSum() {
        Integer [] first = {1,2,3,4,5,6};
        Integer [] second = {1,1,1,1};

        Integer sumResult = dataJoiner.join(Arrays.stream(first), Arrays.stream(second), Integer::sum);
        Assertions.assertEquals(25, (int) sumResult);
    }

    @Test
    void testJoinSumFail() {
        Integer [] first = {1,2,3,4,5,6};
        Integer [] second = {1,1,1,1,1};

        Integer sumResult = dataJoiner.join(Arrays.stream(first), Arrays.stream(second), (a, b) -> a + b);
        Assertions.assertNotEquals(25, (int) sumResult);
    }

    @Test
    void testJoinMultiply() {
        Integer [] first = {1,2,3,4,5,6};
        Integer [] second = {1,1,1,1};

        Integer multiplyResult = dataJoiner.join(Arrays.stream(first), Arrays.stream(second), (a, b) -> a * b);
        Assertions.assertEquals(720, (int) multiplyResult);
    }

    @Test
    void testJoinMultiplyFail() {
        Integer [] first = {1,2,3,4,5,6};
        Integer [] second = {1,1,4};

        Integer multiplyResult = dataJoiner.join(Arrays.stream(first), Arrays.stream(second), (a, b) -> a * b);
        Assertions.assertNotEquals(270, (int) multiplyResult);
    }
}
