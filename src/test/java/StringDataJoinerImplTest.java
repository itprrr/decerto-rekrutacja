import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ExtendWith(MockitoExtension.class)
class StringDataJoinerImplTest {

    private final DataJoiner<String> dataJoiner = new DataJoinerImpl<>();

    @Test
    void testJoinConcatenation() {
        List<String> firstList = Stream.of("one", "two", "three").collect(Collectors.toList());
        List<String> secondList = Stream.of("four", "42").collect(Collectors.toList());

        String result = dataJoiner.join(firstList.stream(), secondList.stream(), (a, b) -> a + "-" + b);
        Assertions.assertEquals(result, "one-two-three-four-42");
    }

    @Test
    void testJoinConcatenationFail() {
        List<String> firstList = Stream.of("one", "two", "three").collect(Collectors.toList());
        List<String> secondList = Stream.of("four", "42").collect(Collectors.toList());

        String result = dataJoiner.join(firstList.stream(), secondList.stream(), (a, b) -> a + "-" + b);
        Assertions.assertNotEquals(result, "one-two-three-four-4i4");
    }

}
