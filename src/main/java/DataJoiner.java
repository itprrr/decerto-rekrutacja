import java.util.function.BinaryOperator;
import java.util.stream.Stream;

public interface DataJoiner<T> {
    T join(Stream<T> firstSource, Stream<T> secondSource, BinaryOperator<T> accumulator);
}
