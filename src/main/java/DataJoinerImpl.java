import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;

public class DataJoinerImpl<T> implements DataJoiner<T> {

    @Override
    public T join(Stream<T> firstSource, Stream<T> secondSource, BinaryOperator<T> accumulator) {

        Stream<T> mergedStream = Stream.concat(firstSource, secondSource);

        Optional<T> reducer = mergedStream.reduce(accumulator);
        return reducer.orElseGet(() -> reducer.orElseThrow(() -> new RuntimeException("Wrong accumulator provided")));
    }
}
